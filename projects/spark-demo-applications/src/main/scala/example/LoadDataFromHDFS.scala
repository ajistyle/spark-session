package example

import org.apache.spark.sql.SparkSession

object LoadDataFromHDFS {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("Load Data From HDFS")
      .getOrCreate()

    val sc = spark.sparkContext
    val lines = sc.textFile("hdfs://spark01:9000/client-ids.log")
    val idsStr = lines.map(line => line.split(","))
    idsStr.first
    idsStr.collect

    val ids = lines.flatMap(_.split(","))
    ids.collect

    val intIds = ids.map(_.toInt)
    intIds.collect

    val uniqueIds = intIds.distinct
    uniqueIds.count
    ids.count
  }
}
