package example

import org.apache.spark.sql.SparkSession

object HelloWorld {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("First Spark Application")
      .master("local[*]")
      .getOrCreate()

    val sc = spark.sparkContext

    val numbers = sc.parallelize(10 to 50 by 10)
    numbers.foreach(x => println(x))

    val numbersSquared = numbers.map(num => num * num)
    numbersSquared.foreach(x => println(x))

    val reversed = numbersSquared.map(x => x.toString.reverse)
    reversed.foreach(x => println(x))
  }
}

