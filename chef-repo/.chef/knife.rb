# for knife zero bootstrap
chef_repo_path    File.expand_path('../' , __FILE__)
node_path         Dir["nodes/**/"]
role_path         "roles"
environment_path  "environments"
data_bag_path     "data-bags"
cookbook_path     ["cookbooks"]
local_mode        true

knife[:ssh_attribute] = "knife_zero.host"
knife[:use_sudo] = true
knife[:identity_file] = "#{ENV["HOME"]}/.vagrant.d/insecure_private_key"
knife[:automatic_attribute_whitelist] = []
knife[:default_attribute_whitelist] = []