spark_version = node["spark"]["version"]
hadoop_version = node["hadoop"]["version"].split(".")[0..1].join(".")

archive = "spark-#{spark_version}-bin-hadoop#{hadoop_version}.tgz"
mirror = "http://mirrors.hust.edu.cn/apache/spark/spark-#{spark_version}/#{archive}"

# Construct the target directory by removing the extention name.
target = "/opt/spark/#{::File::basename(archive, ".tgz")}"

directory "/opt/spark/archives" do
  owner "vagrant"
  group "vagrant"
  mode "0755"
  recursive true
end

remote_file "/opt/spark/archives/#{archive}" do
  source mirror
  owner "vagrant"
  group "vagrant"
  mode "0755"
  not_if { ::File.exist?("/opt/spark/archives/#{archive}") }
end

# extract the archive if necessary
bash 'extract_spark_archive' do
  cwd "/opt/spark"
  code <<-EOH
    tar xzf archives/#{archive} -C #{::File::dirname(target)}
    chown -R vagrant:vagrant #{target}
  EOH
  not_if { ::File.exist?(target) }
end

# create the current symbolic link
link "/opt/spark/current" do
  owner "vagrant"
  group "vagrant"
  to target
end

# config spark
%w(
  docker.properties log4j.properties slaves spark-env.sh fairscheduler.xml
  metrics.properties spark-defaults.conf
).each do |f|
  template "/opt/spark/current/conf/#{f}" do
    source "conf/#{f}.erb"
  end
end
