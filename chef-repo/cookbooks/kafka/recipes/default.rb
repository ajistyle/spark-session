kafka_version = node["kafka"]["version"]

archive = "kafka_2.11-#{kafka_version}.tgz"
mirror = "http://mirrors.shu.edu.cn/apache/kafka/#{kafka_version}/#{archive}"


# Construct the target directory by removing the extention name.
target = "/opt/kafka/#{::File::basename(archive, ".tgz")}"

directory "/opt/kafka/archives" do
  owner "vagrant"
  group "vagrant"
  mode "0755"
  recursive true
end

remote_file "/opt/kafka/archives/#{archive}" do
  source mirror
  owner "vagrant"
  group "vagrant"
  mode "0755"
  not_if { ::File.exist?("/opt/kafka/archives/#{archive}") }
end

# extract the archive if necessary
bash 'extract_kafka_archive' do
  cwd "/opt/kafka"
  code <<-EOH
    tar xzf archives/#{archive} -C #{::File::dirname(target)}
    chown -R vagrant:vagrant #{target}
  EOH
  not_if { ::File.exist?(target) }
end

# create the current symbolic link
link "/opt/kafka/current" do
  owner "vagrant"
  group "vagrant"
  to target
end

# Create data directories
%w(broker zookeeper).each do |x|
  directory node["kafka"]["data_dir"][x] do
    owner "vagrant"
    group "vagrant"
    recursive true
  end
end

# Config zookeeper & broker
%w(server.properties zookeeper.properties).each do |f|
  template "/opt/kafka/current/config/#{f}" do
    source "config/#{f}.erb"
    owner "vagrant"
    group "vagrant"
  end
end

template "#{node["kafka"]["data_dir"]["zookeeper"]}/myid" do
  source "config/myid.erb"
  owner "vagrant"
  group "vagrant"
end
