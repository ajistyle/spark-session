class Chef::Recipe
    class Profile
      class << self
        def user_paths node, user
          profile = node["user"][user]["profile"]
          paths profile
        end

        def user_environments node, user
          profile = node["user"][user]["profile"]
          environments profile
        end

        def system_environments node
          profile = node["system"]["profile"]
          environments profile
        end

        def environments profile
          unless profile.nil? or profile.read("environments").nil?
            profile["environments"].map { |k, v|
              "export #{k}=#{v}"
            }.join("\n")
          end
        end

        def paths profile
          unless profile.nil? or profile.read("paths").nil?
            "export PATH=#{(profile["paths"].to_a + ["$PATH"]).join(":")}"
          end
        end
      end
    end
  end
  