class Chef::Recipe
  class Firewall
    class << self
      # View/ERB helper
      def format_rules rules
        rules.map { |rule|
          <<~EOS
            <rule family="#{rule["family"]}">
              #{rule["spec"].map { |k, v|
                "<#{k} #{v.map { |vk, vv| "#{vk}=\"#{vv}\"" }.join(" ")} />"
              }.join("\n#{" " * 2}")}
              <#{rule["action"]}/>
            </rule>
          EOS
        }.join("\n")
      end
    end
  end
end
