def home username; username.eql?("root") ? "/root" : "/home/#{username}"; end

node["user"].each do |username, profile|
  authorized_keys = []

  query = profile["authorization"].map{ |id| "id:#{id}" }.join(" OR ")
  search(:users, query) do |u|
    authorized_keys += u["ssh-keys"]
  end
  
  user_home = home(username)

  user username do
    shell "/bin/bash"
    home user_home
  end

  directory user_home do
    group username
    owner username
  end

  directory "#{user_home}/.ssh" do
    recursive true
    group username
    owner username
    mode "0700"
  end

  file "#{user_home}/.ssh/authorized_keys" do
    content authorized_keys.join("\n")
    group username
    owner username
    mode "0600"
  end

  template "#{user_home}/.bashrc" do
    source "bashrc.erb"
    group username
    owner username
    variables user: username
  end

  case node["platform"]
  when "ubuntu"
    template "#{user_home}/.profile" do
      source "profile.erb"
      owner username
      group username
    end
  when "centos"
    template "#{user_home}/.bash_profile" do
      source "bash_profile.erb"
      owner username
      group username
    end
  end
end


node["group"].each do |name, m| 
  group name do
    members m
  end
end
