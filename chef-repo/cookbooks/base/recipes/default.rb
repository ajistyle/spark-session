case node['platform']
when 'ubuntu'
  include_recipe 'base::ubuntu'
when 'centos'
  include_recipe 'base::centos'
end

["unzip", "wget", "bash-completion", "bridge-utils", "jq"].each do |pkg|
  package pkg
end

template '/etc/hosts' do
  source 'hosts.erb'
end

%w(user firewall).each do |r| 
  include_recipe "base::#{r}"
end

selinux = node.read("selinux")

if selinux
  template "/etc/selinux/config" do
    source "selinux-config.erb"
    variables selinux: selinux
  end  
end

