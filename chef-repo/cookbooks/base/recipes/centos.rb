['epel-release', 'net-tools'].each do |pkg|
  package pkg
end

cookbook_file '/etc/environment' do
  source 'environment'
end

service "firewalld" do
  action :disable
end
