cookbook_file "/etc/default/locale" do
  source "locale"
end

apt_update 'update'

apt_package "openjdk-8-jdk"

template '/etc/environment' do
  source 'environment.erb'
end