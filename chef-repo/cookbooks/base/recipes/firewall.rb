package "firewalld"

%w(iptables ufw).each do |s|
  systemd_unit "#{s}.service" do
    action [:stop, :disable, :unmask, :delete]
    notifies :run, "execute[systemctl daemon-reload]", :delayed
  end
end

systemd_unit "firewalld.service" do
  action [:enable, :start]
  notifies :run, "execute[systemctl daemon-reload]", :delayed
end

execute "systemctl daemon-reload" do
  command "systemctl daemon-reload"
  action :nothing
end

template "/etc/firewalld/zones/public.xml" do
  source "firewall/public.xml.erb"
  notifies :run, "execute[update-firewall-settings]", :delayed
end

execute "update-firewall-settings" do
  command "firewall-cmd --reload && firewall-cmd --set-default-zone=public"
  action :nothing
end
