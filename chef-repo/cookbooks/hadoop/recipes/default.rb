hadoop_version = node["hadoop"]["version"]

archive = "hadoop-#{hadoop_version}.tar.gz"
mirror = "http://mirror.bit.edu.cn/apache/hadoop/common/hadoop-#{hadoop_version}/#{archive}"

# Construct the target directory by removing the extention name.
target = "/opt/hadoop/#{::File::basename(archive, ".tar.gz")}"

directory "/opt/hadoop/archives" do
  owner "vagrant"
  group "vagrant"
  mode "0755"
  recursive true
end

remote_file "/opt/hadoop/archives/#{archive}" do
  source mirror
  owner "vagrant"
  group "vagrant"
  mode "0755"
  not_if { ::File.exist?("/opt/hadoop/archives/#{archive}") }
end

# extract the archive if necessary
bash 'extract_hadoop_archive' do
  cwd "/opt/hadoop"
  code <<-EOH
    tar xzf archives/#{archive} -C #{::File::dirname(target)}
    chown -R vagrant:vagrant #{target}
  EOH
  not_if { ::File.exist?(target) }
end

# create the current symbolic link
link "/opt/hadoop/current" do
  owner "vagrant"
  group "vagrant"
  to target
end

(node["hadoop"]["data_dir"] + [node["hadoop"]["name_dir"]]).each do |d|
  directory d do
    owner "vagrant"
    group "vagrant"
    mode "0755"
    recursive true
  end
end

["core-site.xml", "hdfs-site.xml", "slaves"].each do |f|
  template "/opt/hadoop/current/etc/hadoop/#{f}" do
    source "#{f}.erb"
    owner "vagrant"
    group "vagrant"
  end
end
