cluster = [
  { host: "spark01", ip: "10.10.11.11", box: "ubuntu/bionic64", mem: 2048, cpus: 2, gui: false },
  { host: "spark02", ip: "10.10.11.12", box: "ubuntu/bionic64", mem: 2048, cpus: 2, gui: false },
  { host: "spark03", ip: "10.10.11.13", box: "ubuntu/bionic64", mem: 2048, cpus: 2, gui: false }
]

Vagrant.configure(2) do |config|
  # Use vagrant-hostmanager for host file management on both host & guest machines
  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.manage_guest = false
  config.hostmanager.ignore_private_ip = false
  config.hostmanager.include_offline = true

  config.vbguest.auto_update = true
  # config.vbguest.no_remote = true

  # The default options is true, as the consequence:
  # 1. private key is generated per machine basis 
  #    @ .vagrant/machines/#{machine_name}/private_key
  # 2. the corresponding public key is install to the machine
  #    @ /home/vagrant/.ssh/authorized_keys

  # If we set the this option to false, the default insecure private key will be used
  # @ ~/.vagrant.d/insecure_private_key
  # the corresponding insecure public key is @ https://github.com/mitchellh/vagrant/blob/master/keys/vagrant.pub

  config.ssh.insert_key = false

  cluster.each do |node|
    config.vm.define node[:host] do |node_config|
      node_config.vm.box = node[:box]

      node_config.vm.hostname = node[:host]
      node_config.hostmanager.aliases = node[:host]

      node_config.vm.network "private_network", ip: node[:ip]
      config.vm.synced_folder "#{ENV["HOME"]}/vm-mount", "/vagrant"

      node_config.vm.provider "virtualbox" do |vb|
        # Config the friendly display name of the node shown in VirtualBox
        vb.name = node[:name]

        # Display the VirtualBox GUI when booting the machine
        vb.gui = node[:gui]

        # Customize the amount of memory on the VM:
        vb.memory = node[:mem]

        # Customize the number of CPU cores on the VM:
        vb.cpus = node[:cpus]
      end
    end
  end
end
